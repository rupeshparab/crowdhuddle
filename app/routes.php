<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return Redirect::to('/projects');
});
//

Route::get('api', ['before' => 'oauth', function() {
 	// return the protected resource
 	//echo “success authentication”;
 	$user_id=Authorizer::getResourceOwnerId(); // the token user_id
 	$user=User::find($user_id);// get the user data from database
	return Response::json($user);
}]);

Route::get('api/projects/', ['before' => 'oauth', function() {
 	// return the protected resource
 	//echo “success authentication”;
 	$user_id=Authorizer::getResourceOwnerId(); // the token user_id
 	$projects=User::find($user_id)->projects;// get the user data from database
	return Response::json($projects);
}]);

Route::post('oauth/access_token', function() {
	return Response::json(Authorizer::issueAccessToken());
});

//

// Confide RESTful route
Route::get('users/confirm/{code}', 'UsersController@getConfirm');
Route::get('users/reset_password/{token}', 'UsersController@getReset');
Route::get('users/reset_password', 'UsersController@postReset');
Route::controller('users', 'UsersController');


Route::resource('projects', 'ProjectsController');
Route::get('project/new', 'ProjectsController@create');
Route::get('project/manage', 'ProjectsController@manage');
Route::get('project/update', 'ProjectsController@updation');
Route::get('project/delete', 'ProjectsController@delete');


Route::post('project/postdelete', array('as' => 'projectdelete',
    'uses' => 'ProjectsController@postDelete'));
Route::post('project/postupdate', array('as' => 'projectupdate',
    'uses' => 'ProjectsController@postUpdate'));
Route::post('project/destroy', array('as' => 'projectdestroy',
    'uses' => 'ProjectsController@destroy'));
Route::post('project/updates', array('as' => 'updates',
    'uses' => 'ProjectsController@update'));
Route::post('project/store', array('as' => 'projectcreate',
    'uses' => 'ProjectsController@store'));

Route::post('/like', function()
{
	if(!Auth::guest())
	{
		$pro_id = Input::get('pro_id');
		$user_id = Input::get('user_id');
		if(count(DB::table('feedback')->where('user_id',$user_id)->where('pro_id',$pro_id)->get()) > 0)
			DB::table('feedback')->where('user_id',$user_id)->where('pro_id',$pro_id)->update(array('action' => '1'));
		else
			DB::table('feedback')->insert(array('user_id' => $user_id, 'pro_id' => $pro_id, 'action' => '1'));
	}
	return Redirect::back();
});

Route::post('/flag', function()
{
	if(!Auth::guest())
	{
		$pro_id = Input::get('pro_id');
		$user_id = Input::get('user_id');
		if(count(DB::table('feedback')->where('user_id',$user_id)->where('pro_id',$pro_id)->get()) > 0)
			DB::table('feedback')->where('user_id',$user_id)->where('pro_id',$pro_id)->update(array('action' => '0'));
		else
			DB::table('feedback')->insert(array('user_id' => $user_id, 'pro_id' => $pro_id, 'action' => '0'));
	}
	return Redirect::back();
});

Route::group(array('prefix' => 'admin'), function(){
	Route::resource('users', 'AdminUsersController');
});

Route::get('/pay/{id}', ['as' => 'pay', 'uses' => 'RewardsController@getPay']);
Route::post('/rewards/{id}', ['as' => 'rew.pay', 'uses' => 'RewardsController@postPay']);

Route::get('/chat/{id}', 'ChatController@chat');

Route::get('/dash', 'AdminController@dash');
