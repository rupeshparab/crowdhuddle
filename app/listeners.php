<?php
/**
 * Created by PhpStorm.
 * User: Rupesh Parab
 * Date: 23-03-2015
 * Time: 21:40
 */

Event::listen(UpdateScoreEventHandler::EVENT, 'UpdateScoreEventHandler');
Event::listen(ProjectUpdatedEventHandler::EVENT, 'ProjectUpdatedEventHandler');
Event::listen(ChatEventHandler::EVENT, 'ChatEventHandler');