<?php

class RewardsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /rewards
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /rewards/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /rewards
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /rewards/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /rewards/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /rewards/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /rewards/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function getPay($id)
	{
		$rew = DB::table('rewards')->where('id',$id)->first();
		return View::make('payment.payment')->with('rew', $rew);
	}

	public function postPay($id)
	{
		$user = Input::get('user_id');
		$rew = DB::table('rewards')->where('id',$id)->first();
		DB::table('trans')->insert(array('rew_id' => $id, 'user_id' => $user));
		if($rew->inventory!=-1)
			DB::table('rewards')->where('id',$id)->decrement('inventory');
		DB::table('rewards')->where('id',$rew->id)->increment('backers');
		DB::table('projects')->where('id',$rew->pro_id)->increment('funds_coll',$rew->amount);
		$collected = DB::table('projects')->where('id',$rew->pro_id)->first()->funds_coll;
		$target = DB::table('projects')->where('id',$rew->pro_id)->first()->target;
		$progress = $collected*100/$target;
		DB::table('projects')->where('id', $rew->pro_id)->update(array('progress' => $progress));
		$progress = DB::table('projects')->where('id', $rew->pro_id)->first()->progress;
		Event::fire(ProjectUpdatedEventHandler::EVENT, json_encode(array('id_ab' => $rew->pro_id."ab", 'id_cd' => $rew->pro_id."cd", 'id_coll' => $rew->pro_id."coll", 'id_pro' => $rew->pro_id."pro", 'collected' => "Collected: ".$collected, 'progress' => $progress."%", 'progress_text' => "Progress: ".$progress."%")));
		return Redirect::to('/projects/'.$rew->pro_id);
	}

}