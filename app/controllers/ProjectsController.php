<?php

class ProjectsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /projects
	 *
	 * @return Response
	 */
	public function index()
	{
		$projects = Project::paginate(12);
		return View::make('project.index')->with('projects', $projects);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /projects/create
	 *
	 * @return Response
	 */
	public function create()
	{
		
		return View::make('project.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /projects
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::only('pname','categoryid','atp','description','target','deadline','progress');
		//Input Rewards
		$validator1=ProjectCreate::validate($input);
		if ($validator1->fails())
       {
        //Check Condition for rewards
        //Store in database
       }
       else{
       	//Direct to create page
       }
	}

	/**
	 * Display the specified resource.
	 * GET /projects/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$project = Project::where('id', $id)->first();
		return View::make('project.single')->with('project',$project);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /projects/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /projects/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$dt=new DateTime;
		$var = Session::get('useremail');
		$userid = DB::table('users')->where('username', '$userid')->pluck('id');
		$id = DB::table('project_updates')->insertGetId(
	    	array('pro_id' => '$proid', 'user_id' => '$userid', 'description' => '$projectdescription','created_at' =>'$dt','updated_at'=>'$dt')
	    ); 
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /projects/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		DB::table('projects')->where('id', '=', $id)->delete();
	}

	public function manage()
	{
		return View::make('project.myproject');
	}

	 public function updation()
	{
		return View::make('project.update');	
	}
	public function postUpdate(){
	  
       $input=Input::all();
       $projectid=$input['projectid'];
       $projectdescription=$input['description'];
	   $validator=ProjectDelete::validate($input);
       if ($validator->fails())
       {
        return Redirect::action('ProjectsController@updation')->withInput($input)->withErrors($validator->getMessageBag());;
       }
       else{
       	$this->update($projectid,$projectdescription);
       }
	   
	}

    public function delete()
	{
	   return View::make('project.delete');		
	}

    public function postDelete()
	{
	   $input=Input::all();
	   $validator=ProjectDelete::validate($input);
	   $id=$input['projectid'];
       if ($validator->fails())
       {
        return Redirect::action('ProjectsController@delete')->withInput($input)->withErrors($validator->getMessageBag());
       }
       else{
       $this->destroy($id);
       }
	   
      
      
	}

}