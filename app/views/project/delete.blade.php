@extends('partials.layout')

@section('css')
<link rel="stylesheet" href="{{ url('css/style.css',[], !App::isLocal()) }}">
@stop


  

@section('content')
{{--*/ $var = Session::get('useremail'); /*--}}

@if ( $errors->count() > 0 )
      <p>The following errors have occurred:</p>

      <ul>
        @foreach( $errors->all() as $message )
          <li>{{ $message }}</li>
        @endforeach
      </ul>
    @endif

{{ Form::open(array('action' => array('ProjectsController@postDelete'))) }}
<table cellspacing="20">
<tr>
<td>
Select A Project:
 	      	</td>
 	      	<td>
 	      		<select name="projectid" value="{{{ Input::old('projectid') }}}">
                    {{--*/ $projects = DB::table('projects')
            ->join('projects_users', 'projects.id', '=', 'projects_users.pro_id')
            ->join('users', 'users.id', '=', 'projects_users.user_id')
            ->where('users.username', '=', $var)
            ->get(); /*--}}                    
 	      			@foreach($projects as $project)
                    <option value="{{$project->id}}">{{$project->name}} </option>       
 	      			@endforeach
 	      			
                </select>
 	      	</td>	
</tr>
<tr>
<td>
	Description:
</td>
<td>
<textarea  name="description" value="{{{ Input::old('description') }}}"rows="5" cols="60"> </textarea>   
</td>
</tr>
<tr>
	<td>
	<input type="submit" value="Delete"></input>
</td>
</tr>
</table>   	
{{ Form::close() }}    


@stop
@section('foot-js')
<script>
	$(document).ready( function() {

		$('.gallery-item').hover( function() {
			$(this).find('.img-title').fadeIn(300);
		}, function() {
			$(this).find('.img-title').fadeOut(100);
		});

	});
</script>
@stop

