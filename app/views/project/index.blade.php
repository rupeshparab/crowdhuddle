@extends('partials.layout')
@section('css')
<link rel="stylesheet" href="{{ url('css/style.css',[], !App::isLocal()) }}">
@stop

@section('content')
<?php $categories = DB::table('project_category')->get(); ?>
<section class="cd-gallery wrapper clearfix">
	<ul style="text-align: justify;">
		@foreach($projects as $project)
		<li class="mix {{ DB::table('project_category')->where('id',$project->cat_id)->pluck('name')}}" name="{{$project->name}}">
			<figure class="gallery-item">
			<a href="/projects/{{$project->id}}">
				<img src="{{ asset('img/img.png') }}" alt="Image 1">
					<figcaption class="img-title">
						<h5>
							<br/>Category: {{ DB::table('project_category')->where('id',$project->cat_id)->pluck('display_name')}}<br/>
							<br/>Deadline: {{$project->deadline}}<br/>
							<br/>Target: {{$project->target}}<br/>
							<br/><div id="{{$project->id}}coll">Collected: {{$project->funds_coll}}</div>
							<br/><div id="{{$project->id}}pro">Progress: {{$project->progress}}%</div></br>
							<br/>
							<div class="project-progress-bar">
								<div id="{{$project->id}}cd" class="project-percent-pledged" style="width: {{($project->progress > 100)?100:$project->progress}}%"></div>
							</div>
							<br/>
						</h5>
					</figcaption>
				</a>
				<p>{{$project->name}}</p>
				<div class="project-progress-bar">
					<div id="{{$project->id}}ab" class="project-percent-pledged" style="width: {{($project->progress > 100)?100:$project->progress}}%"></div>
				</div>
			</figure>
		</li>
		@endforeach
		<li class="gap"></li>
		<li class="gap"></li>
		<li class="gap"></li>
	</ul>
	<div class="cd-fail-message">No results found</div>
</section> <!-- cd-gallery -->
{{$projects->links()}}
<div class="cd-filter">
	<form>
		<div class="cd-filter-block">
			<h4>Search</h4>

			<div class="cd-filter-content">
				<input type="search" placeholder="Try dicta...">
			</div> <!-- cd-filter-content -->
		</div> <!-- cd-filter-block -->

		<div class="cd-filter-block">
			<h4>Multiple Genres</h4>

			<ul class="cd-filter-content cd-filters list">
				@foreach($categories as $cat)
				<li>
					<input class="filter" data-filter=".{{$cat->name}}" type="checkbox" id="{{$cat->name}}">
					<label class="checkbox-label" for="{{$cat->name}}">{{$cat->display_name}}</label>
				</li>
				@endforeach

			</ul> <!-- cd-filter-content -->
		</div> <!-- cd-filter-block -->

	</form>

	<a href="#0" class="cd-close">Close</a>
</div> <!-- cd-filter -->

<a href="#0" class="cd-filter-trigger">Search</a>
@stop


@section('foot-js')
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.10.2.min.js"><\/script>')</script>
<script src="js/bootstrap.js"></script>
<script src="{{ asset('js/socket.io-1.3.4.js') }}"></script>
<script type="text/javascript">

	$(document).ready( function() {

		$('.gallery-item').hover( function() {
			$(this).find('.img-title').fadeIn(300);
		}, function() {
			$(this).find('.img-title').fadeOut(100);
		});

	});

	var socket = io.connect('http://crowdhuddle.co:3000/');

    socket.on('connect', function(data){
        socket.emit('subscribe', {channel:'score.update'});
    });

    socket.on('score.update', function (data) {
        //Do something with data
        console.log('Score updated: ', data);
    });

    socket.on('projects.update', function (data) {
        //Do something with data
        var obj = JSON.parse(data);
        document.getElementById(obj.id_ab).style.width = obj.progress;
        document.getElementById(obj.id_cd).style.width = obj.progress;
        document.getElementById(obj.id_pro).innerHTML = obj.progress_text;
        document.getElementById(obj.id_coll).innerHTML = obj.collected;

        console.log('Project updated: ', typeof obj.id);

    });
    // ]]>
</script>
@stop
