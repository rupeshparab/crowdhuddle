@extends('partials.layout')

@section('css')
<link rel="stylesheet" href="{{ url('css/tabbed_nav.css',[], !App::isLocal()) }}">
@stop

@section('content')
{{ Form::open(array('action' => array('ProjectsController@store'))) }}
<div class="cd-tabs">
    <nav>
        <ul class="cd-tabs-navigation">
            <li><a data-content="step1" class="selected" href="#0">Basics</a></li>
            <li><a data-content="step2" href="#0">Description</a></li>
            <li><a data-content="step3" href="#0">Target</a></li>
            <li><a data-content="step4" href="#0">Reward</a></li>
            </ul> <!-- cd-tabs-navigation -->
    </nav>

    <ul class="cd-tabs-content">
        <li data-content="step1" class="selected">
           <table cellspacing="20">
            <tr>
            <td>Project Name:</td>
            <td>
                <input   type="text" placeholder=" eg. Crowd Huddle" name="pname" value="{{{ Input::old('pname') }}}" />
            </td>   
            </tr>
            <tr>
            <td>Project Category:</td>
            <td>
                <select name="categoryid">
                    <?php $categories = DB::table('project_category')->get(); ?>
                    
                    @foreach($categories as $cat)
                    <option value="{{$cat->id}}">{{$cat->name}} </option>       
                    @endforeach
                </select>
            </td>   
            </tr>
             </table>   
           </li>

        <li data-content="step2">
        <table cellspacing="20">
            <tr> 
            <td>
            About The Project   
            </td>   
            <td>
            <textarea  name="atp" rows="10" cols="100"> </textarea>   
            </td>
            </tr>
            
            <tr> 
            <td>
            How much work is done?  
            </td>   
            <td>
            <textarea  name="description" rows="10" cols="100"> </textarea>   
            </td>
            </tr>
            
            </table>
            
        </li>

        <li data-content="step3">
        <table cellspacing="20">
        <tr>
            <td>
                Target:
            </td>
            <td>
                <input   type="text" placeholder="$10000" name="target" value="{{{ Input::old('target') }}}" />
            </td>   
        </tr>
        <tr>
            <td>
                Deadline
            </td>
            <td>
                <input   type="date"  name="deadline" value="{{{ Input::old('deadline') }}}" />
            </td>   
            </tr>
        <tr>
            <td>
                Approximate Progress (%)
            </td>
            <td>
                <input   type="text" placeholder=" eg. 25%" name="progress" value="{{{ Input::old('progress') }}}" />
            </td>   
            </tr>
                    

        </table>             
        </li>

        <li data-content="step4">
            <div id="rewards">
    Reward 1 <input type="button" id="add_reward()" onClick="addReward()" value="Add" />
    </br>
    Name: <input type="text" name="rname1"><br />
    Amount: <input type="text" name="ramount1"><br />
    Description:<textarea  name="des1" rows="5" cols="50"> </textarea>
    </br>    
    Backers: <input type="text" name="backers1">
    </div>
    </br>
            <input type="submit" value="CREATE"></input>
        </li>

        </ul> <!-- cd-tabs-content -->
</div> <!-- cd-tabs -->
{{ Form::close() }}
@stop