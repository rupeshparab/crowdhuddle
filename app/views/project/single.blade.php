@extends('partials.layout')
@section('css')
<link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="{{ url('css/reset.css',[], !App::isLocal()) }}">
<link rel="stylesheet" href="{{ url('css/exp-view.css',[], !App::isLocal()) }}">
<link rel="stylesheet" href="{{ url('css/style.css',[], !App::isLocal()) }}">
<link rel="stylesheet" href="{{ url('css/button.css',[], !App::isLocal()) }}">
{{ HTML::script('js/modernizr.js') }}
@stop


@section('content')
	<header>
		<h1>{{$project->name}}</h1>
	</header>
	<section class="cd-single-item">
		<div class="cd-slider-wrapper">
			<ul class="cd-slider">
				<li class="selected"><img src="{{ url('img/img-1.jpg',[], !App::isLocal()) }}" alt="Product Image 1"></li>
				<li><img src="{{ url('img/img-2.jpg',[], !App::isLocal()) }}"  alt="Product Image 2"></li>
				<li><img src="{{ url('img/img-3.jpg',[], !App::isLocal()) }}"  alt="Product Image 3"></li>
			</ul> <!-- cd-slider -->

			<ul class="cd-slider-navigation">
				<li><a href="#0" class="cd-prev inactive">Next</a></li>
				<li><a href="#0" class="cd-next">Prev</a></li>
			</ul> <!-- cd-slider-navigation -->

			<a href="#0" class="cd-close">Close</a>
		</div> <!-- cd-slider-wrapper -->

		<div class="cd-item-info">

			<p>Target: {{$project->target}}</p>
			<p>Collected: {{$project->funds_coll}}</p>
			<p>Progress: {{$project->progress}}%</p>
			<p>Deadline: {{$project->deadline}}</p>

			<button onclick="location.href='/chat/{{$project->id}}'" class="add-to-cart">Chat</button>						
		</div> <!-- cd-item-info -->
	</section> <!-- cd-single-item -->

	<section class="cd-content">
		<h2>Updates</h2>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum voluptatem, quisquam veniam sequi in quasi excepturi laudantium fugit nihil odio minima quae consequuntur dolorum pariatur obcaecati, adipisci dignissimos officia saepe itaque deleniti porro odit vitae voluptate. Blanditiis sunt obcaecati corporis, alias adipisci. Eum illum voluptatibus expedita nulla eius provident pariatur!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum voluptatem, quisquam veniam sequi in quasi excepturi laudantium fugit nihil odio minima quae consequuntur dolorum pariatur obcaecati, adipisci dignissimos officia saepe itaque deleniti porro odit vitae voluptate.</p>
	</section>

	<?php $rews = DB::table('rewards')->where('pro_id',$project->id)->get(); ?>
	<section style="min-height: {{(Auth::guest())?(ceil(count($rews)/4))*10 + 25 : (ceil(count($rews)/4))*15 + 30}}vw">
		<h2>Rewards</h2>
		<div>
		<section class="cd-gallery wrapper clearfix" style="
padding-right: 50px;">
	<ul style="text-align: justify;">
			
			@foreach($rews as $rew)
			<li class="mix">
			<p class="reward" style="height: {{(Auth::guest())?'15vw':'20vw'}};">
				{{{$rew->name}}}</br>
				Desc: {{{$rew->desc}}}</br>
				@if($rew->inventory!=-1)
				{{{$rew->inventory}}} items left</br>
				@endif
				Price: {{{$rew->amount}}}</br>
				Backers: {{{$rew->backers}}}</br>
				
				</br>
			</p>
			@if(!Auth::guest())
				<br>
				<a href="/pay/{{$rew->id}}" class="reward-button-9">Pay</a>
				</br>
				@endif
			</li>
			@endforeach
		<li class="gap"></li>
		<li class="gap"></li>
		<li class="gap"></li>
	</ul>
</section> <!-- cd-gallery -->
@stop

@section('foot-js')	
{{ HTML::script('js/jquery-2.1.1.js') }}
{{ HTML::script('js/jquery.mobile.min.js') }}
{{ HTML::script('js/exp-view.js') }}
@stop