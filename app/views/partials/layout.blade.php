<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="{{ url('css/font.css',[], !App::isLocal()) }}">
	<link rel="stylesheet" href="{{ url('css/anim-text.css',[], !App::isLocal()) }}">
	<link rel="stylesheet" href="{{ url('css/nav.css',[], !App::isLocal()) }}"> <!-- CSS reset -->

	<link rel="stylesheet" href="{{ url('css/reset.css',[], !App::isLocal()) }}"> <!-- CSS reset --> <!-- Resource style -->
	@yield('css')
	<script src="{{ url('js/modernizr.js',[], !App::isLocal()) }}"></script> <!-- Modernizr -->

	<title>Home</title>
</head>
<body>
	<header class="cd-header">
		<section class="cd-intro ">
		<h1 class="cd-headline letters type" href="/projects"><span>Crowd</span>
			<b class="is-visible">Huddle</b>
			<b>Funding</b>
		</h1>
		</section>
	</header>

	<main class="cd-main-content">
		<div class="cd-tab-nav-wrapper">
			@if(Auth::guest())
			<div class="cd-tab-nav">
				<a  {{(Request::is('users/create'))?'class="selected"':''}} href="/users/create">Register</a>
			</div> <!-- cd-tab-nav -->
			<div class="cd-tab-nav">
				<a {{(Request::is('users/login'))?'class="selected"':''}} href="/users/login">Login</a>
			</div> 
			@else
				@if(Auth::user()->hasRole('admin'))
				<div class="cd-tab-nav">
					<a {{(Request::is('dash*'))?'class="selected"':''}} href="/dash">Dash</a>
				</div>
				@else
				<div class="cd-tab-nav large">
					<a {{(Request::is('project/new*'))?'class="selected"':''}} href="/project/new">My Account</a>
				</div>
				@endif
				<div class="cd-tab-nav large">
					<a {{(Request::is('project/new*'))?'class="selected"':''}} href="/project/new">Create New</a>
				</div>
			@endif
			<div class="cd-tab-nav">
				<a {{(Request::is('projects*'))?'class="selected"':''}} href="/projects">Browse</a>
			</div>
		</div> <!-- cd-tab-nav-wrapper -->

		@yield('content')

	</main> <!-- cd-main-content -->
	{{ HTML::script('js/jquery-2.1.1.js') }}
	{{ HTML::script('js/jquery.mixitup.min.js') }}
	{{ HTML::script('js/main.js') }}
	{{ HTML::script('js/velocity.min.js') }}
	{{ HTML::script('js/anim-text.js') }}
	@yield('foot-js')
</body>
</html>


