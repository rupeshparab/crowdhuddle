<!DOCTYPE html>
<!--[if lt IE 7]>	  <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>		 <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>		 <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/bootstrap-responsive.min.css" type="text/css" media="screen">
    <script src="js/modernizr-2.6.2.min.js"></script>
</head>
<body>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12" id="chat-app">
                <h3>
                    Chat Setup Guide - Rupesh Parab
                </h3>
                <h5>
                    <p>To go to the project click here: <u><a href="/projects">Project</a></u>
                        <p>I am writing this guide to setup the chat server</p>

                        <p>log into vagrant vm using <u>vagrant ssh</u> command and go into project directory</p>
                        <p>run the following commands:
                            <ul>
                                <li><u>composer update</u> :To get the BrainSocket Library</li>
                                <li><u>php artisan brainsocket:start --port 8081</u> :To start the server</li>
                            </ul>
                            <p>To test:
                                <ol>
                                    <li>Open multiple tabs</li>
                                    <li>Enter text in Input Box</li>
                                    <li>Press Enter</li>
                                </ol>
                            </p>
                        </h5>
                        <form class="form-inline">
                            <input type="text" class="input" id="chat-message" placeholder="Type a message and hit enter.">
                        </form>
                        <div id="chat-log">

                        </div>
                    </div>
                </div>
            </div>

            <!--@scripts start-->
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
            <script>window.jQuery || document.write('<script src="js/jquery-1.10.2.min.js"><\/script>')</script>
            <script src="js/bootstrap.js"></script>
            <script src="https://cdn.socket.io/socket.io-1.3.4.js"></script>

<script type="text/javascript">// <![CDATA[
    var socket = io.connect('http://crowdhuddle.co:3000/');

    socket.on('connect', function(data){
        socket.emit('subscribe', {channel:'score.update'});
    });

    socket.on('score.update', function (data) {
        //Do something with data
        console.log('Score updated: ', data);
    });

    socket.on('projects.update', function (data) {
        //Do something with data
        console.log('Score updated: ', data);
    });

    // ]]>
</script>
</body>
</html>