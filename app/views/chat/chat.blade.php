<!DOCTYPE html>
<!--[if lt IE 7]>	  <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>		 <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>		 <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}" type="text/css" media="screen">
	<link rel="stylesheet" href="{{ asset('css/bootstrap-responsive.min.css') }}" type="text/css" media="screen">

	{{ HTML::script('js/modernizr-2.6.2.min.js') }}
</head>
<body>
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12" id="chat-app">
			<br/>
			Project: {{$project->name}}
			<br/><br/>
				<form class="form-inline">
					<input type="text" class="input" id="chat-message" placeholder="Type a message and hit enter.">
				</form>
                <div id="chat-log">

                </div>
			</div>
		</div>
	</div>

	<!--@scripts start-->

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/jquery-1.10.2.min.js"><\/script>')</script>
	{{ HTML::script('js/bootstrap.js') }}
	<script src="{{ asset('js/socket.io-1.3.4.js') }}"></script>
	<script type="text/javascript">
		$(function(){
			var socket = io.connect('http://192.168.10.10:3000/');


			var user_name = "{{Auth::user()->name}}";
			var project_id = "{{$project->id}}"
			//make sure to update the port number if your ws server is running on a different one.
			window.app = {};

			socket.on('chat message',function(data){
				console.log('Score updated: ', data);
				if(data.user_name == user_name){
					$('#chat-log').append('<div class="alert alert-success">Me: '+data.message+'</div>');
				}else if(data.project_id == project_id){
					$('#chat-log').append('<div class="alert alert-info">'+data.user_name+': '+data.message+'</div>');
				}
			});
			$('#chat-message').keypress(function(event) {
						if(event.keyCode == 13){
							console.log('Emit 1');
							socket.emit('chat message',
									{
										'message':$(this).val(),
										'user_name':user_name,
										'project_id':project_id
									}
							);
							console.log('Emit 2');
							$(this).val('');
						}
						return event.keyCode != 13; }
			);
		});
	</script>
</body>
</html>
