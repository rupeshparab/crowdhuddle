




<?php
	$presenter = new Illuminate\Pagination\BootstrapPresenter($paginator);
?>

<?php if ($paginator->getLastPage() > 1): ?>
	<nav role="navigation">
		<ul class="cd-pagination no-space move-buttons custom-icons">
			<?php echo $presenter->render(); ?>
		</ul>
	</nav>
<?php endif; ?>