<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl">
<head>
  <meta charset="UTF-8">
  <title>Crowd Huddle Payment Portal</title>
  <meta http-equiv="Expires" content="0"/>
  <meta name="robots" content="noindex, nofollow"/>
  <meta name="author" content=""/>
  <meta name="language" content=""/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"  />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />

  <link rel="stylesheet" href="data/b_b_d_a_c18d962cae63b83d7ddf19f8ca12e1391a7e.css" type="text/css"/>

  <script type="text/javascript"  src="data/js_previewLibs_0fdac5e6.js"></script>

  <script>
    (function() {
      window.uxpft = new FeatureToggler({"features_names":{"FEATURE_LAYERS":"feature_layers","FEATURE_UXPATTERNS":"feature_uxpatterns","FEATURE_UXDOCUMENTS":"feature_uxdocuments","FEATURE_DOCUMENTS_UPLOAD":"feature_documents_upload","FEATURE_CHAT":"feature_chat","FEATURE_EXPORTHTML":"feature_export_html","FEATURE_PREVIEWPASS":"feature_previewpass","FEATURE_EXTENDEDLIBRARY":"feature_extendedlibrary","FEATURE_HIGHER_PLAN_INFO":"feature_higher_plan_info","FEATURE_LIVE_SHARE":"feature_live_share","FEATURE_PROJECT_IMPORT":"feature_project_import","FEATURE_USER_TESTING":"feature_user_testing","FEATURE_VAT_PER_COUNTRY":"feature_vat_per_country","FEATURE_GROUP_DOCUMENTS":"feature_group_documents"},"features_status":{"feature_vat_per_country":true},"features_by_plan":[]});
    })();
  </script>

  <link rel="shortcut icon" href="/favicon.ico">
  <link rel="icon" type="image/png" href="data/favicon.png">


  <script type="text/javascript">
        //potrzebna dla floating sidebara :/
        dpManager = null;
        if (navigator.appName == 'Microsoft Internet Explorer') {
          alert("We're trying hard to make UXPin work with InternetExplorer, but we're not there yet. Meanwhile please use Firefox, Chrome or Safari");
        }
      </script>

    </head><body class="preview html-export">


    <script type="text/javascript"  src="data/js_offlinePreviewLibs_7313eff1.js"></script>
    <link rel="stylesheet" href="data/e_d_e_1_d6d3d2b3e5bb4d3397b62d9049312dcd78f6.css" type="text/css"/>
    <script>
      var Preview = new dmsDPPreview_Preview();
    </script>

    <div id="canvas">
      <div id="canvas-wrapper">
        <div id="canvas-area">
          <div id="main1">
            <div class="Component " style="relative-left:0;relative-top:0;z-index:10001;id_component:15911861;font-family:;border-width:0px;border-style:solid;width:680px;height:465px;position:absolute;top:50%;left:50%;
            margin:-232px 0 0 -340px;" id="el105982_829184fa4bd2a24e14863149dd984400">
            <div class="ElBox  old-box" style="left:0px;top:0px;z-index:10001;border-radius:0px;border-color:#bfbcbb;border-style:solid;background-color:#ffffff;background-image:url('');background-repeat:no-repeat;color:#000000;font-size:14px;font-weight:normal;font-style:normal;font-family:Arial;text-align:center;border-top-width:1px;border-right-width:1px;border-bottom-width:1px;border-left-width:1px;border-top-left-radius:10px;border-top-right-radius:10px;width:672px;height:72px;position:absolute;background-position:left top" id="el10598211123_1d54ecee270b657b0f302f54dd042864" >
            </div>
            <script type='text/javascript' data-evaluate='1'>(function() {Preview.mElements.set(10598211123, document.getElementById('el10598211123_1d54ecee270b657b0f302f54dd042864'), {"id":11123,"interactive":false,"in_group":false,"in_component":true,"id_component":105982})})();</script>
            <div class="ElBox  old-box" style="left:0px;top:71px;z-index:10002;border-radius:0px;border-color:#bfbcbb;border-style:solid;background-color:#f0daf5;background-image:url('');background-repeat:no-repeat;color:#000000;font-size:14px;font-weight:normal;font-style:normal;font-family:Arial;text-align:center;border-top-width:1px;border-right-width:1px;border-bottom-width:1px;border-left-width:1px;border-bottom-left-radius:10px;border-bottom-right-radius:10px;width:672px;height:394px;position:absolute;background-position:left top" id="el10598244429_27962c34f45ee96b0051445e609a7bbe" ></div>
            <script type='text/javascript' data-evaluate='1'>(function() {Preview.mElements.set(10598244429, document.getElementById('el10598244429_27962c34f45ee96b0051445e609a7bbe'), {"id":44429,"interactive":false,"in_group":false,"in_component":true,"id_component":105982})})();</script>
            <div class="ElBox  old-box" style="left:174px;top:138px;z-index:10003;border-radius:0px;border-color:#bfbcbb;border-style:solid;background-color:#ffffff;background-image:url('');background-repeat:no-repeat;color:#000000;font-size:14px;font-weight:normal;font-style:normal;font-family:Arial;text-align:center;border-top-width:1px;border-right-width:1px;border-bottom-width:1px;border-left-width:1px;border-top-left-radius:5px;border-top-right-radius:5px;border-bottom-right-radius:5px;border-bottom-left-radius:5px;width:323px;height:210px;position:absolute;background-position:left top" id="el1059821656_57daae5b8101a517d2fcf37c7c7e15bd" ></div>
            <script type='text/javascript' data-evaluate='1'>(function() {Preview.mElements.set(1059821656, document.getElementById('el1059821656_57daae5b8101a517d2fcf37c7c7e15bd'), {"id":1656,"interactive":false,"in_group":false,"in_component":true,"id_component":105982})})();</script>
            <div class="ElBox  old-box" style="left:179px;top:150px;z-index:10004;border-width:0px;border-radius:0px;border-color:#888;border-style:solid;background-color:#ebebeb;background-image:url('');background-repeat:no-repeat;color:#000000;font-size:14px;font-weight:normal;font-style:normal;font-family:Arial;text-align:center;width:315px;height:39px;position:absolute;background-position:left top" id="el10598244228_d18c7c72986498ab72d08e7458a1d2b3" ></div>
            <script type='text/javascript' data-evaluate='1'>(function() {Preview.mElements.set(10598244228, document.getElementById('el10598244228_d18c7c72986498ab72d08e7458a1d2b3'), {"id":44228,"interactive":false,"in_group":false,"in_component":true,"id_component":105982})})();</script>
            <div class="ElText " style="line-height:130%;left:187px;top:160px;z-index:10005;overflow:hidden;border-width:0px;border-radius:0px;border-color:#000000;border-style:solid;background-color:transparent;background-image:url('');background-repeat:no-repeat;color:#791f7a;font-size:17px;font-family:Trebuchet MS;font-weight:bold;text-align:left;width:136px;height:44px;position:absolute;background-position:left top" id="el10598266028_2906012a0f71ecb8808c97164a6fe11e" >Payment details</div>
            <script type='text/javascript' data-evaluate='1'>(function() {Preview.mElements.set(10598266028, document.getElementById('el10598266028_2906012a0f71ecb8808c97164a6fe11e'), {"id":66028,"interactive":false,"in_group":false,"in_component":true,"id_component":105982})})();</script>
            <div id="el1059822015_88962f517d7399b65e4d50db320d249e" class=" ElSvgIcon" style=left:451px;top:156px;z-index:10024;color:#808080;font-family:;border-width:0px;border-style:solid;width:28px;height:28px;position:absolute;  >
              <div style="width:100%;height:100%;position:absolute;top: 0;left: 0;"></div>
              <div data-dupa="http://app.uxpin.comdata/elements_icons_svg_credit_card.svg.svg" class="e159157742015"><?xml version="1.0" encoding="utf-8"?>
                <!-- Generator: Adobe Illustrator 15.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                width="28px" height="28px" viewBox="0 0 256 256" enable-background="new 0 0 256 256" xml:space="preserve">
                <path fill="#343434" d="M234.747,21.31c5.837,0,10.853,2.097,15.019,6.234c4.166,4.194,6.234,9.238,6.234,15.16v170.563
                c0,5.837-2.068,10.881-6.234,15.075c-4.166,4.222-9.182,6.348-15.019,6.348H21.395c-5.837,0-10.881-2.097-15.075-6.263
                C2.097,224.262,0,219.218,0,213.268V42.704c0-5.837,2.068-10.853,6.234-15.075c4.166-4.222,9.209-6.319,15.16-6.319H234.747
                L234.747,21.31z M234.747,42.704H21.395v37.207h213.353V42.704z M234.747,133.412H21.395v79.855h213.353V133.412z M78.75,199.949
                H36.102v-15.982H78.75V199.949z M155.855,199.949H93.91v-15.982h61.945V199.949z"/>
              </svg>
            </div>
          </div>

          <script type='text/javascript' data-evaluate='1'>(function() {Preview.mElements.set(1059822015, document.getElementById('el1059822015_88962f517d7399b65e4d50db320d249e'), {"id":2015,"interactive":false,"in_group":false,"in_component":true,"id_component":105982})})();</script>
          <div class="ElBox  old-box" style="left:186px;top:226px;z-index:10006;border-radius:0px;border-color:#c2c2c2;border-style:solid;background-color:#ffffff;background-image:url('');background-repeat:no-repeat;color:#000000;font-size:14px;font-weight:normal;font-style:normal;font-family:Arial;text-align:center;border-top-width:1px;border-right-width:1px;border-bottom-width:1px;border-left-width:1px;width:297px;height:36px;position:absolute;background-position:left top" id="el10598247702_1026fc0c45e8a3ce59d7c8a01d476e65" ></div>
          <script type='text/javascript' data-evaluate='1'>(function() {Preview.mElements.set(10598247702, document.getElementById('el10598247702_1026fc0c45e8a3ce59d7c8a01d476e65'), {"id":47702,"interactive":false,"in_group":false,"in_component":true,"id_component":105982})})();</script>
          <div class="ElBox  old-box" style="left:188px;top:299px;z-index:10007;border-radius:0px;border-color:#c2c2c2;border-style:solid;background-color:#ffffff;background-image:url('');background-repeat:no-repeat;color:#000000;font-size:14px;font-weight:normal;font-style:normal;font-family:Arial;text-align:center;border-top-width:1px;border-right-width:1px;border-bottom-width:1px;border-left-width:1px;width:52px;height:36px;position:absolute;background-position:left top" id="el10598280640_2db90bc72761d307c58084288141eb39" ></div>
          <script type='text/javascript' data-evaluate='1'>(function() {Preview.mElements.set(10598280640, document.getElementById('el10598280640_2db90bc72761d307c58084288141eb39'), {"id":80640,"interactive":false,"in_group":false,"in_component":true,"id_component":105982})})();</script>
          <div class="ElBox  old-box" style="left:247px;top:299px;z-index:10008;border-radius:0px;border-color:#c2c2c2;border-style:solid;background-color:#ffffff;background-image:url('');background-repeat:no-repeat;color:#000000;font-size:14px;font-weight:normal;font-style:normal;font-family:Arial;text-align:center;border-top-width:1px;border-right-width:1px;border-bottom-width:1px;border-left-width:1px;width:52px;height:36px;position:absolute;background-position:left top" id="el10598290681_c8f1cb1c87d97389ed14668fd551e384" ></div>
          <script type='text/javascript' data-evaluate='1'>(function() {Preview.mElements.set(10598290681, document.getElementById('el10598290681_c8f1cb1c87d97389ed14668fd551e384'), {"id":90681,"interactive":false,"in_group":false,"in_component":true,"id_component":105982})})();</script>
          <div class="ElBox  old-box" style="left: 420px;top: 299px;z-index: 10009;border-radius: 0px;border-color: #c2c2c2;border-style: solid;background-color: #ffffff;background-image: url('');background-repeat: no-repeat;color: #000000;font-size: 14px;font-weight: normal;font-style: normal;font-family: Arial;text-align: center;border-top-width: 1px;border-right-width: 1px;border-bottom-width: 1px;border-left-width: 1px;width: 63px;height: 36px;position: absolute;background-position: left top;" id="el10598294452_6eafb732178d832218ac7ff77aead2b6" ></div>
          <script type='text/javascript' data-evaluate='1'>(function() {Preview.mElements.set(10598294452, document.getElementById('el10598294452_6eafb732178d832218ac7ff77aead2b6'), {"id":94452,"interactive":false,"in_group":false,"in_component":true,"id_component":105982})})();</script>
          <div class="ElText " style="line-height:130%;left:187px;top:202px;z-index:10010;overflow:hidden;border-width:0px;border-radius:0px;border-color:#000000;border-style:solid;background-color:transparent;background-image:url('');background-repeat:no-repeat;color:#960992;font-size:12px;font-family:Arial;font-weight:normal;text-align:left;width:101px;height:15px;position:absolute;background-position:left top" id="el1059828069_19b3971fdceb788c5523446d8a2f32dd" >CARD NUMBER</div>
          <script type='text/javascript' data-evaluate='1'>(function() {Preview.mElements.set(1059828069, document.getElementById('el1059828069_19b3971fdceb788c5523446d8a2f32dd'), {"id":8069,"interactive":false,"in_group":false,"in_component":true,"id_component":105982})})();</script>
          <div id="el10598234443_e2f9ffcd34ab02e8fb0e1a33cb66fa87" class=" ElSvgIcon" style=left:456px;top:232px;z-index:10011;color:#ebebeb;font-family:;border-width:0px;border-style:solid;width:20px;height:20px;position:absolute;  >
            <div style="width:100%;height:100%;position:absolute;top: 0;left: 0;"></div>
            <div data-dupa="http://app.uxpin.comdata/elements_icons_svg_lock.svg.svg" class="e1591577434443"><?xml version="1.0" encoding="utf-8"?>
              <!-- Generator: Adobe Illustrator 15.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
              <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
              <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
              width="20px" height="20px" viewBox="0 0 256 256" enable-background="new 0 0 256 256" xml:space="preserve">
              <path fill="#343434" d="M217.769,116.47c3.8,1.3,6.865,3.599,9.131,6.865c2.333,3.266,3.466,7.032,3.466,11.263v102.274
              c0,5.131-1.933,9.597-5.665,13.396c-3.732,3.8-8.197,5.732-13.33,5.732H44.764c-5.132,0-9.598-1.933-13.396-5.732
              c-3.833-3.799-5.732-8.265-5.732-13.396V134.598c0-4.231,1.167-7.997,3.5-11.263c2.366-3.266,5.432-5.565,9.264-6.865V89.677
              c0-12.297,2.333-23.894,6.998-34.791c4.699-10.897,11.097-20.428,19.195-28.593c8.131-8.165,17.662-14.562,28.592-19.261
              S115.778,0,128.075,0s23.894,2.333,34.773,7.032c10.931,4.699,20.462,11.097,28.594,19.261c8.197,8.165,14.596,17.729,19.328,28.692
              c4.665,10.931,6.998,22.495,6.998,34.691V116.47L217.769,116.47z M179.179,89.677c0-7.165-1.333-13.896-3.999-20.095
              c-2.666-6.198-6.332-11.597-10.997-16.162c-4.683-4.565-10.147-8.198-16.413-10.864c-6.265-2.666-12.83-3.999-19.694-3.999
              c-7.198,0-13.83,1.333-19.928,3.999s-11.53,6.298-16.262,10.864c-4.732,4.565-8.431,9.998-11.098,16.263
              c-2.666,6.231-3.999,12.93-3.999,19.995v25.493h102.39V89.677z M139.038,182.619c3.134-1.933,5.732-4.599,7.732-7.897
              c1.999-3.333,3.032-6.898,3.032-10.698c0-5.998-2.133-11.163-6.365-15.529c-4.265-4.365-9.397-6.531-15.362-6.531
              c-5.999,0-11.097,2.166-15.363,6.531c-4.266,4.366-6.365,9.531-6.365,15.529c0,3.8,0.966,7.365,2.933,10.698
              c1.967,3.299,4.499,5.831,7.698,7.598l-10.631,46.888h43.456L139.038,182.619z"/>
            </svg>
          </div>
        </div>

        <script type='text/javascript' data-evaluate='1'>(function() {Preview.mElements.set(10598234443, document.getElementById('el10598234443_e2f9ffcd34ab02e8fb0e1a33cb66fa87'), {"id":34443,"interactive":false,"in_group":false,"in_component":true,"id_component":105982})})();</script>
        <div class="ElText " style="line-height:130%;left:189px;top:276px;z-index:10012;overflow:hidden;border-width:0px;border-radius:0px;border-color:#000000;border-style:solid;background-color:transparent;background-image:url('');background-repeat:no-repeat;color:#8b1391;font-size:12px;font-family:Arial;font-weight:normal;text-align:left;width:101px;height:15px;position:absolute;background-position:left top" id="el10598267806_84f9c6063912df0c392acd01fba56cb3" >EXPIRE DATE</div>
        <script type='text/javascript' data-evaluate='1'>(function() {Preview.mElements.set(10598267806, document.getElementById('el10598267806_84f9c6063912df0c392acd01fba56cb3'), {"id":67806,"interactive":false,"in_group":false,"in_component":true,"id_component":105982})})();</script>
        <div class="ElText " style="line-height:130%;left:419px;top:276px;z-index:10013;overflow:hidden;border-width:0px;border-radius:0px;border-color:#000000;border-style:solid;background-color:transparent;background-image:url('');background-repeat:no-repeat;color:#8d1b9e;font-size:12px;font-family:Arial;font-weight:normal;text-align:left;width:62px;height:30px;position:absolute;background-position:left top" id="el10598286630_b8b42d8820cd611f55c0118ad927a496" >CV CODE</div>
        <script type='text/javascript' data-evaluate='1'>(function() {Preview.mElements.set(10598286630, document.getElementById('el10598286630_b8b42d8820cd611f55c0118ad927a496'), {"id":86630,"interactive":false,"in_group":false,"in_component":true,"id_component":105982})})();</script>    

      <div id="el1059822743_3a134419f6e30dacbddc54bdad191673" class="GeoSVGEl " style="left:525px;top:373px;z-index:10015;color:#aa78ab;stroke:3;element:triangle;font-family:;border-width:0px;border-style:solid;width:50px;height:21px;position:absolute;-o-transform:rotate(270deg);-ms-transform:rotate(270deg);-moz-transform:rotate(270deg);-webkit-transform:rotate(270deg);-webkit-transform-origin: 25px 10.5px;-moz-transform-origin: 25px 10.5px;-ms-transform-origin: 25px 10.5px;-o-transform-origin: 25px 10.5px;">
       <svg version="1.2" width="100%" height="100%">
        <polygon class="shape" points="0,100 50,0, 100,100" fill="#aa78ab" style="-webkit-transform: scale(0.5,0.21); -webkit-transform-origin: 0% 0%; -moz-transform: scale(0.5,0.21); -moz-transform-origin: 0% 0%; -ms-transform: scale(0.5,0.21); -ms-transform-origin: 0% 0%; -o-transform: scale(0.5,0.21); -o-transform-origin: 0% 0%;" />
      </svg>
    </div>
    <script type='text/javascript' data-evaluate='1'>(function() {Preview.mElements.set(1059822743, document.getElementById('el1059822743_3a134419f6e30dacbddc54bdad191673'), {"id":2743,"interactive":false,"in_group":false,"in_component":true,"id_component":105982})})();</script>
    <div class="ElBox  old-box" style="left:560px;top:358px;z-index:10016;border-width:0px;border-radius:0px;border-color:#888;border-style:solid;background-color:#aa78ab;background-image:url('');background-repeat:no-repeat;color:#000000;font-size:14px;font-weight:normal;font-style:normal;font-family:Arial;text-align:center;width:121px;height:50px;position:absolute;background-position:left top" id="el10598222962_9da0b4c2d9e13b689c8e13daa68bbf6c" ></div>
    <script type='text/javascript' data-evaluate='1'>(function() {Preview.mElements.set(10598222962, document.getElementById('el10598222962_9da0b4c2d9e13b689c8e13daa68bbf6c'), {"id":22962,"interactive":false,"in_group":false,"in_component":true,"id_component":105982})})();</script>
    <div id="el10598246035_ccc39251346063e429eadb136b345947" class="GeoSVGEl " style="left: 673px;top: 403px;z-index:10017;color:#775478;stroke:3;element:triangle2;font-family:;border-width:0px;border-style:solid;width:7px;height:10px;position:absolute;-webkit-transform-origin: 4px 5px;-moz-transform-origin: 4px 5px;-ms-transform-origin: 4px 5px;-o-transform-origin: 4px 5px;">
     <svg version="1.2" width="100%" height="100%">
      <polygon class="shape" points="100,0 0,0, 0,100" fill="#775478" style="-webkit-transform: scale(0.08,0.1); -webkit-transform-origin: 0% 0%; -moz-transform: scale(0.08,0.1); -moz-transform-origin: 0% 0%; -ms-transform: scale(0.08,0.1); -ms-transform-origin: 0% 0%; -o-transform: scale(0.08,0.1); -o-transform-origin: 0% 0%;" />
    </svg>
  </div>
  <script type='text/javascript' data-evaluate='1'>(function() {Preview.mElements.set(10598246035, document.getElementById('el10598246035_ccc39251346063e429eadb136b345947'), {"id":46035,"interactive":false,"in_group":false,"in_component":true,"id_component":105982})})();</script>
  <div class="ElText " style="line-height:130%;left:593px;top:370px;z-index:10018;overflow:hidden;border-width:0px;border-radius:0px;border-color:#000000;border-style:solid;background-color:transparent;background-image:url('');background-repeat:no-repeat;color:#ffffff;font-size:20px;font-family:Arial;font-weight:bold;text-align:left;width:79px;height:26px;position:absolute;background-position:left top" id="el10598278619_78c6beb0211b48d3cf9bfc8edbba45bf" >₹ {{$rew->amount}}</div>
  <form method="POST" action="/rewards/{{$rew->id}}" accept-charset="UTF-8">
    <input type="hidden" name="user_id" value="{{{ Auth::id() }}}">
    <script type='text/javascript' data-evaluate='1'>(function() {Preview.mElements.set(10598278619, document.getElementById('el10598278619_78c6beb0211b48d3cf9bfc8edbba45bf'), {"id":78619,"interactive":false,"in_group":false,"in_component":true,"id_component":105982})})();</script>
    <input type="text" name="card" id="card"  class="ElText " placeholder="XXXXXXXXXXXXXXXX" style="line-height: 130%;left: 186px;top: 228px;z-index: 10019;overflow: hidden;border-width: 0px;border-radius: 0px;border-color: #000000;border-style: solid;background-color: transparent;background-image: url('');background-repeat: no-repeat;color: #808080;font-size: 16px;font-family: Arial;font-weight: normal;text-align: left;width: 278px;height: 37px;padding: 0px 20px;position: absolute;outline: none;background-position: left top;" id="el10598211815_3c7c3ff4c01818953d561a0f73f45f21" ></input>
    <input type="text" name="month" id="month" class="ElText " placeholder="MM"  style="line-height:130%;left:201px;top:307px;z-index:10020;overflow:hidden;border-width:0px;border-radius:0px;border-color:#000000;border-style:solid;background-color:transparent;background-image:url('');background-repeat:no-repeat;color:#808080;font-size:16px;font-family:Arial;font-weight:normal;text-align:left;width:27px;height:20px;position:absolute;background-position:left top;outline: none;" id="el10598236339_ca7e83f02749b899407d8bd1c06ed107" ></input>
    <script type='text/javascript' data-evaluate='1'>(function() {Preview.mElements.set(10598236339, document.getElementById('el10598236339_ca7e83f02749b899407d8bd1c06ed107'), {"id":36339,"interactive":false,"in_group":false,"in_component":true,"id_component":105982})})();</script>
    <input type="text" name="year" id="year" class="ElText " placeholder="YY" style="line-height:130%;left:260px;top:307px;z-index:10021;overflow:hidden;border-width:0px;border-radius:0px;border-color:#000000;border-style:solid;background-color:transparent;background-image:url('');background-repeat:no-repeat;color:#808080;font-size:16px;font-family:Arial;font-weight:normal;text-align:left;width:27px;height:20px;position:absolute;background-position:left top;outline: none;" id="el10598247919_8e5c7bc0e1a07a2d15572f9349b6df41" ></input>
    <script type='text/javascript' data-evaluate='1'>(function() {Preview.mElements.set(10598247919, document.getElementById('el10598247919_8e5c7bc0e1a07a2d15572f9349b6df41'), {"id":47919,"interactive":false,"in_group":false,"in_component":true,"id_component":105982})})();</script>
    <input class="ElText " name="cvv" id="cvv" placeholder="XXX"  style="line-height: 130%;left: 433px;top: 307px;z-index: 10022;overflow: hidden;border-width: 0px;border-radius: 0px;border-color: #000000;border-style: solid;background-color: transparent;background-image: url('');background-repeat: no-repeat;color: #808080;font-size: 16px;font-family: Arial;font-weight: normal;text-align: left;width: 38px;height: 20px;position: absolute;background-position: left top;outline: none;" id="el10598256516_7671281ef99dc8d87616c148d0f1b94f" ></input>
    <script type='text/javascript' data-evaluate='1'>(function() {Preview.mElements.set(10598256516, document.getElementById('el10598256516_7671281ef99dc8d87616c148d0f1b94f'), {"id":56516,"interactive":false,"in_group":false,"in_component":true,"id_component":105982})})();</script>
     <input  type="submit" style="background-image: linear-gradient(bottom, #7a0d77 0%, #92108f 100%);background-image: -o-linear-gradient(bottom, #7a0d77 0%, #92108f 100%);background-image: -moz-linear-gradient(bottom, #7a0d77 0%, #92108f 100%);background-image: -webkit-linear-gradient(bottom, #7a0d77 0%, #92108f 100%);background-image: -ms-linear-gradient(bottom, #7a0d77 0%, #92108f 100%);left:175px;top:363px;z-index:10014;border-width:0px;border-radius:0px;border-color:#f222eb;border-style:solid;background-color:#7a0d77;color:#fafafa;font-size:18px;font-weight:normal;font-style:normal;font-family:Arial;text-align:none;border-top-left-radius:5px;border-top-right-radius:5px;border-bottom-right-radius:5px;border-bottom-left-radius:5px;box-shadow:;-o-box-shadow:;-moz-box-shadow:;-webkit-box-shadow:;background-image:url('');width:323px;height:40px;position:absolute;outline:none;" class="ElButton " value="Pay" id="el1059825670_da6c0990e2cb9cbf3e6f8a7ebeaf8576" />

      <script type='text/javascript' data-evaluate='1'>(function() {Preview.mElements.set(1059825670, document.getElementById('el1059825670_da6c0990e2cb9cbf3e6f8a7ebeaf8576'), {"id":5670,"interactive":false,"in_group":false,"in_component":true,"id_component":105982})})();</script>
      </form>
    <div class="ElText " style="line-height:130%;left:21px;top:26px;z-index:10023;overflow:hidden;border-width:0px;border-radius:0px;border-color:#000000;border-style:solid;background-color:transparent;background-image:url('');background-repeat:no-repeat;color:#4d4a4a;font-size:18px;font-family:Georgia;font-weight:bold;text-align:left;width:622px;height:46px;position:absolute;background-position:left top" id="el10598212706_d72b435f67dd1284ba854ca9efab6989" >{{$rew->name}} ({{DB::table('projects')->where('id',$rew->pro_id)->first()->name}})</div>
    <script type='text/javascript' data-evaluate='1'>(function() {Preview.mElements.set(10598212706, document.getElementById('el10598212706_d72b435f67dd1284ba854ca9efab6989'), {"id":12706,"interactive":false,"in_group":false,"in_component":true,"id_component":105982})})();</script></div>
    <script type='text/javascript' data-evaluate='1'>(function() {Preview.mElements.set(105982, document.getElementById('el105982_829184fa4bd2a24e14863149dd984400'), {"id":105982,"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>
    <script type="text/javascript" data-evaluate="1">
      Interactions.ori_styles = {"105982":{"opacity":null,"width":680,"height":465,"top":100,"left":65},"10598211123":{"opacity":null,"width":672,"height":72,"top":0,"left":0},"10598244429":{"opacity":null,"width":672,"height":394,"top":71,"left":0},"1059821656":{"opacity":null,"width":323,"height":210,"top":138,"left":174},"10598244228":{"opacity":null,"width":315,"height":39,"top":150,"left":177},"10598266028":{"opacity":null,"width":136,"height":44,"top":160,"left":187},"1059822015":{"opacity":null,"width":28,"height":28,"top":156,"left":451},"10598247702":{"opacity":null,"width":297,"height":36,"top":226,"left":186},"10598280640":{"opacity":null,"width":52,"height":36,"top":299,"left":188},"10598290681":{"opacity":null,"width":52,"height":36,"top":299,"left":247},"10598294452":{"opacity":null,"width":52,"height":36,"top":299,"left":431},"1059828069":{"opacity":null,"width":101,"height":15,"top":202,"left":187},"10598234443":{"opacity":null,"width":20,"height":20,"top":232,"left":456},"10598267806":{"opacity":null,"width":101,"height":15,"top":276,"left":189},"10598286630":{"opacity":null,"width":62,"height":30,"top":276,"left":431},"1059825670":{"opacity":null,"width":323,"height":40,"top":363,"left":175},"1059822743":{"opacity":null,"width":50,"height":21,"top":373,"left":553},"10598222962":{"opacity":null,"width":92,"height":50,"top":358,"left":588},"10598246035":{"opacity":null,"width":8,"height":10,"top":407,"left":672},"10598278619":{"opacity":null,"width":79,"height":26,"top":370,"left":593},"10598211815":{"opacity":null,"width":183,"height":20,"top":235,"left":199},"10598236339":{"opacity":null,"width":27,"height":20,"top":307,"left":201},"10598247919":{"opacity":null,"width":27,"height":20,"top":307,"left":260},"10598256516":{"opacity":null,"width":27,"height":20,"top":307,"left":444},"10598212706":{"opacity":null,"width":622,"height":46,"top":26,"left":21}};
      Interactions.scrollable_el = document.getElementById('canvas-wrapper');
      Interactions.timeouts = [];
      Interactions.scrolls = [];
      var load_events = [];



      Interactions.onContentLoaded(load_events);
    </script>

  </div>
</div>
</div>
</div>





</body>
</html>