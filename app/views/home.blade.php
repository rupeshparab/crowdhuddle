@extends('partials.layout')
@section('css')
<link rel="stylesheet" href="{{ asset('css/style.css') }}">
@stop

@section('content')
<?php $categories = DB::table('project_category')->get(); ?>
<section class="cd-gallery wrapper clearfix">
	<ul style="text-align: justify;">
		@foreach($projects as $project)
		<li class="mix {{ DB::table('project_category')->where('id',$project->cat_id)->pluck('name')}}" name="{{$project->name}}">
			<figure class="gallery-item">
				<img src="{{ asset('img/img.png') }}" alt="Image 1">
				<a href="/projects/{{$project->id}}">
				<figcaption class="img-title">
					<h5>
					</br>Category: {{ DB::table('project_category')->where('id',$project->cat_id)->pluck('display_name')}}</br>
				</br>Deadline: {{$project->deadline}}</br>
			</br>Target: {{$project->target}}</br>
		</br>Collected: {{$project->funds_coll}}</br>
	</br>Progress: {{$project->progress}}%</br>
</br>
<div class="project-progress-bar">
	<div  id="{{$project->id}}" class="project-percent-pledged" style="width: {{($project->progress > 100)?100:$project->progress}}%"></div>
</div>
</br>
</h5>
</div>
</figcaption>
				</a>
<p>{{$project->name}}</p>
<div class="project-progress-bar">
	<div id="{{$project->id}}"  class="project-percent-pledged" style="width: {{($project->progress > 100)?100:$project->progress}}%"></div>
</div>
</figure>
</li>
@endforeach
<li class="gap"></li>
<li class="gap"></li>
<li class="gap"></li>

</ul>
<div class="cd-fail-message">No results found</div>
</section> <!-- cd-gallery -->
{{$projects->links()}}
<div class="cd-filter">
	<form>
		<div class="cd-filter-block">
			<h4>Search</h4>

			<div class="cd-filter-content">
				<input type="search" placeholder="Try dicta...">
			</div> <!-- cd-filter-content -->
		</div> <!-- cd-filter-block -->

		<div class="cd-filter-block">
			<h4>Multiple Genres</h4>

			<ul class="cd-filter-content cd-filters list">
				@foreach($categories as $cat)
				<li>
					<input class="filter" data-filter=".{{$cat->name}}" type="checkbox" id="{{$cat->name}}">
					<label class="checkbox-label" for="{{$cat->name}}">{{$cat->display_name}}</label>
				</li>
				@endforeach

			</ul> <!-- cd-filter-content -->
		</div> <!-- cd-filter-block -->

	</form>

	<a href="#0" class="cd-close">Close</a>
</div> <!-- cd-filter -->

<a href="#0" class="cd-filter-trigger">Search</a>
@stop


@section('foot-js')
<script>
	$(document).ready( function() {

		$('.gallery-item').hover( function() {
			$(this).find('.img-title').fadeIn(300);
		}, function() {
			$(this).find('.img-title').fadeOut(100);
		});

	});
	var socket = io.connect('http://crowdhuddle.co:3000/');

    socket.on('connect', function(data){
        socket.emit('subscribe', {channel:'score.update'});
    });

    socket.on('score.update', function (data) {
        //Do something with data
        console.log('Score updated: ', data);
    });

    socket.on('projects.update', function (data) {
        //Do something with data
        document.getElementById(data).style.width = 10;
        console.log('Score updated: ', data);
    });
</script>
@stop
