@extends('partials.layout')

@section('css')
<link rel="stylesheet" href="{{ asset('css/style.css') }}">
<link rel="stylesheet" href="{{ asset('css/button.css') }}">
@stop

@section('content')

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600' rel='stylesheet' type='text/css'>
<link href="//netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/form.css') }}">
<div class="testbox">
  <h1>Registration</h1>
  <hr>

  <form method="POST" action="{{{ URL::to('users') }}}" accept-charset="UTF-8">

    <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">

    <label id="icon" for="username"><i class="icon-user"></i></label>
    <input  type="text" name="username" id="username" value="{{{ Input::old('username') }}}" placeholder="{{{ Lang::get('confide::confide.username') }}}" required/>

    <label id="icon" for="email"><i class="icon-envelope "></i></label>
    <input placeholder="{{{ Lang::get('confide::confide.e_mail') }}}" type="text" name="email" id="email" value="{{{ Input::old('email') }}}" required/>

    <label id="icon" for="password"><i class="icon-shield"></i></label>
    <input placeholder="{{{ Lang::get('confide::confide.password') }}}" type="password" name="password" id="password" required/>

    <label id="icon" for="password_confirmation"><i class="icon-shield"></i></label>
    <input placeholder="{{{ Lang::get('confide::confide.password_confirmation') }}}" type="password" name="password_confirmation" id="password_confirmation" required/>

    <p>By clicking Register, you agree on our <a href="#">terms and condition</a>.</p>

    <input type="submit" class="styled-button-9">


@if (Session::get('error'))
<div class="alert alert-error alert-danger">
  @if (is_array(Session::get('error')))
  {{ head(Session::get('error')) }}
  @endif
</div>
@endif

@if (Session::get('notice'))
<div class="alert">{{ Session::get('notice') }}</div>
@endif
</form>
</div>
@stop
