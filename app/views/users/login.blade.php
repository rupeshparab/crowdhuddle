@extends('partials.layout')

@section('css')
<link rel="stylesheet" href="{{ asset('css/style.css') }}">
<link rel="stylesheet" href="{{ asset('css/button.css') }}">
@stop

@section('content')

<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600' rel='stylesheet' type='text/css'>
<link href="//netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/form.css') }}">
<div class="testbox">
  <h1>Login</h1>
  <hr>

  <form role="form" method="POST" action="{{{ URL::to('/users/login') }}}" accept-charset="UTF-8">

    <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">

    <label id="icon" for="email"><i class="icon-envelope "></i></label>
    <input  tabindex="1" placeholder="{{{ Lang::get('confide::confide.username_e_mail') }}}" type="text" name="email" id="email" value="{{{ Input::old('email') }}}" required/>

    <label id="icon" for="password"><i class="icon-shield"></i></label>
    <input  tabindex="2" placeholder="{{{ Lang::get('confide::confide.password') }}}" type="password" name="password" id="password" required/>

    <p><a href="{{{ URL::to('/users/forgot_password') }}}">{{{ Lang::get('confide::confide.login.forgot_password') }}}</a></p>

      <div class="checkbox">
      <label for="remember">
        <input tabindex="4" type="checkbox" name="remember" id="remember" value="1"> {{{ Lang::get('confide::confide.login.remember') }}}
      </label>
      </div>

      <input type="submit" class="styled-button-9"></input>

    @if (Session::get('error'))
    <div class="alert alert-error alert-danger">{{{ Session::get('error') }}}</div>
    @endif

    @if (Session::get('notice'))
    <div class="alert">{{{ Session::get('notice') }}}</div>
    @endif

  </form>
</div>

@stop
