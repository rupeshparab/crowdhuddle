<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ProjectCategoryTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		DB::table('project_category')->insert(array(
			array('name'=>'art','display_name'=>'Art'),
			array('name'=>'comics','display_name'=>'Comics'),
			array('name'=>'crafts','display_name'=>'Crafts'),
			array('name'=>'design','display_name'=>'Design'),
			array('name'=>'fashion','display_name'=>'Fashion'),
			array('name'=>'filmnvideo','display_name'=>'Film & Video'),
			array('name'=>'games','display_name'=>'Games'),
			array('name'=>'journo','display_name'=>'Journalism'),
			array('name'=>'music','display_name'=>'Music'),
			array('name'=>'photo','display_name'=>'Photography'),
			array('name'=>'tech','display_name'=>'Technology'),
			array('name'=>'theat','display_name'=>'Theater'),
			array('name'=>'other','display_name'=>'Other'),
			)
		);
	}

}