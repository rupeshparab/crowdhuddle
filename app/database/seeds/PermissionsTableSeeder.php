<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class PermissionsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		DB::table('permissions')->insert(array(
			array('name'=>'admin', 'display_name'=>'Super Admin'),
			array('name'=>'p1', 'display_name'=>'P1 perm'),
			array('name'=>'p2', 'display_name'=>'P2 perm'),
			array('name'=>'p3', 'display_name'=>'P3 perm'),
			)
		);
	}

}
