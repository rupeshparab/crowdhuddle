<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class RolesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();
		$now = date('Y-m-d H:i:s');
		DB::table('roles')->insert(array(
			array('name'=>'admin'),
			array('name'=>'moderators'),
			array('name'=>'users'),			)
		);

		DB::table('permission_role')->insert(array(
			array('permission_id'=>'1', 'role_id'=>'1'),
			array('permission_id'=>'2', 'role_id'=>'1'),
			array('permission_id'=>'3', 'role_id'=>'1'),
			array('permission_id'=>'4', 'role_id'=>'1'),
			array('permission_id'=>'2', 'role_id'=>'2'),
			array('permission_id'=>'3', 'role_id'=>'2'),
			array('permission_id'=>'2', 'role_id'=>'2'),
			array('permission_id'=>'3', 'role_id'=>'2'),
			array('permission_id'=>'4', 'role_id'=>'2'),
			array('permission_id'=>'3', 'role_id'=>'3'),
			array('permission_id'=>'4', 'role_id'=>'3')
			)
		);
	}

}
