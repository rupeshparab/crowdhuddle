<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class OauthTableSeederTableSeeder extends Seeder {

	public function run()
	{
		DB::table('oauth_clients')->insert(
		 array('id' => 'f3d259ddd3ed8ff3843839b',
		 			'secret' => '4c7f6f8fa93d59c45502c0ae8c4a95b',
					'name' => 'Main website',
					'created_at' => '2015–05–12 21:00:00',
					'updated_at' => '0000–00–00 00:00:00'
				)
	);
	}

}
