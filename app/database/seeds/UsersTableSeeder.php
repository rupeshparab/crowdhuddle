<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();
		$user = new User;
		$user->name = 'Rupesh Parab';
		$user->username = 'admin';
		$user->email = 'admin@admin.com';
		$user->password = 'admin123';
		$user->password_confirmation = 'admin123';
		$user->confirmation_code = md5(uniqid(mt_rand(), true));
		$now = date('Y-m-d H:i:s');
		$user->created_at = $now;
		$user->updated_at = $now;
		$user->confirmed = 1;
		$user->save();

		DB::table('assigned_roles')->insert(array('user_id'=>'1', 'role_id'=>'1'));

		foreach(range(2, 1000) as $index)
		{
			$user = new User;
			$user->name = $faker->name;
		    $user->username = $faker->userName.mt_rand(1,100);
		    $user->email = $user->username.'@'.$faker->safeEmailDomain;
		    $user->password = 'user123';
		    $user->password_confirmation = 'user123';
		    $user->confirmation_code = md5(uniqid(mt_rand(), true));
			$user->created_at = $now;
			$user->updated_at = $now;
		    $user->confirmed = 1;
		    Log::info('Trying to create '.$user->username.', '.$user->email.', '.$user->confirmation_cod);

		    if(! $user->save()) {
		      Log::info('Unable to create user '.$user->username, (array)$user->errors());
		    }
		    else
		    {
		    	Log::info('Created user "'.$user->username.'" <'.$user->email.'>');
		    	if(mt_rand(1,5)==1)
		    		DB::table('assigned_roles')->insert(array('user_id'=>$index, 'role_id'=>'2'));
		    	else
		    		DB::table('assigned_roles')->insert(array('user_id'=>$index, 'role_id'=>'3'));
		    }


		}
	}

}
