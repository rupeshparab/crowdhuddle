<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		$this->call('PermissionsTableSeeder');
		$this->call('RolesTableSeeder');
		$this->call('UsersTableSeeder');
		$this->call('ProjectCategoryTableSeeder');
		$this->call('ProjectsTableSeeder');
		$this->call('TransTableSeederTableSeeder');
		$this->call('OauthTableSeederTableSeeder');


	}

}
