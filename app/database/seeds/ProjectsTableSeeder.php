<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ProjectsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();
		$now = date('Y-m-d H:i:s');
		foreach(range(1, 150) as $index)
		{
			$project = new Project;
			$temptext = $faker->text(50);
			$project->name = substr($temptext, 0, strlen($temptext)-1);
			$tar = mt_rand(10,100)*pow(10, mt_rand(3,4));
			$project->target = $tar;
			$project->deadline = $faker->dateTimeBetween($startDate = '+30day', $endDate = '+60days');
			$project->cat_id = mt_rand(1,13);
			$project->funds_coll = '0';
			$project->progress = '0';
			$project->created_at = $now;
			$project->updated_at = $now;
			$project->save();

			foreach(range(1, 1000) as $index2)
			{
				if(mt_rand(1,200)==1)
				{
					DB::table('projects_users')->insert(array('pro_id' => $index, 'user_id' => $index2));
				}
			}
			$faker2 = Faker::create();
			foreach(range(1, mt_rand(1,5)) as $index3)
			{
				DB::table('rewards')->insert(array('name' => 'Reward '.mt_rand(1,10000), 'desc' => $faker2->text(50), 'pro_id' => $index, 'amount' => mt_rand(1,10)*pow(10, mt_rand(2,3)), 'inventory' => ((mt_rand(0,2))?mt_rand(10,100):'-1')));
			}

		}
	}

}
