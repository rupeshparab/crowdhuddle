<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class TransTableSeederTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		$rewards = DB::table('rewards')->get();

		foreach($rewards as $reward)
		{
			foreach(range(1,1000) as $index)
			{
				if(mt_rand(1,100)=='1')
				{
					$inv = DB::table('rewards')->where('id',$reward->id)->pluck('inventory');
					if($inv!=0)
					{
						DB::table('trans')->insert(array('rew_id' => $reward->id, 'user_id' => $index));
						if($inv>0)
							DB::table('rewards')->where('id',$reward->id)->decrement('inventory');
						DB::table('rewards')->where('id',$reward->id)->increment('backers');
						DB::table('projects')->where('id',$reward->pro_id)->increment('funds_coll',$reward->amount);
						$collected = DB::table('projects')->where('id',$reward->pro_id)->first()->funds_coll;
						$target = DB::table('projects')->where('id',$reward->pro_id)->first()->target;
						DB::table('projects')->where('id', $reward->pro_id)->update(array('progress' => $collected*100/$target));
					}
				}
			}

		}
	}

}
