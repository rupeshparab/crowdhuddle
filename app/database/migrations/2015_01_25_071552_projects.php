<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Projects extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects', function ($table) {
            $table->increments('id')->unsigned();
            $table->string('name')->unique();
            $table->bigInteger('target');
            $table->integer('cat_id')->unsigned();
            $table->date('deadline');
            $table->integer('progress')->unsigned()->default('0');
            $table->bigInteger('funds_coll');
            $table->timestamps();
            $table->integer('flags')->unsigned()->default('0');
            $table->integer('likes')->unsigned()->default('0');
            $table->foreign('cat_id')->references('id')->on('project_category');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('projects');
	}

}
