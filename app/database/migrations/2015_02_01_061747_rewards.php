<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Rewards extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rewards', function ($table) {
            $table->increments('id')->unsigned();
            $table->string('name', 100);
            $table->integer('pro_id')->unsigned();
            $table->text('desc');
            $table->mediumInteger('amount')->unsigned();
            $table->mediumInteger('inventory')->nullable();
            $table->mediumInteger('backers')->unsigned()->default('0');
            $table->timestamps();
            $table->foreign('pro_id')->references('id')->on('projects')->onDelete('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rewards');
	}

}
