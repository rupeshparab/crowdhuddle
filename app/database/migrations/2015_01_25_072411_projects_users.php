<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProjectsUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects_users', function ($table) {
            //$table->increments('id')->unsigned();
            $table->integer('pro_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
            $table->primary(array('pro_id', 'user_id'));
            $table->foreign('pro_id')->references('id')->on('projects');
            $table->foreign('user_id')->references('id')->on('users');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('projects_users');
	}

}
