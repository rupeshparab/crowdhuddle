<?php

use Zizaco\Confide\UserValidator as ConfideUserValidator;
use Zizaco\Confide\UserValidatorInterface;

class UserValidator extends ConfideUserValidator implements UserValidatorInterface
{
    public $rules = [
        'create' => [
            'email'    => 'required|email',
            'password' => 'required|min:4',
        ],
        'update' => [
            'email'    => 'required|email',
            'password' => 'required|min:4',
        ]
    ];
}
