<?php

use Zizaco\Confide\ConfideUser;
use Zizaco\Confide\ConfideUserInterface;
use Zizaco\Entrust\HasRole;

class User extends Eloquent implements ConfideUserInterface
{
    use ConfideUser, HasRole;

  	protected $guarded = array('id', 'password');

    public function projects()
  	{
  		return $this->belongsToMany('Project', 'projects_users', 'user_id', 'pro_id');
  	}
}
