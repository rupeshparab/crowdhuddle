<?php

class Project extends \Eloquent {
	protected $table = 'projects';
	protected $fillable = ['name','target','cat_id','deadline','progress','fund_coll'];

	public function users()
	{
		return $this->belongsToMany('User');
	}

	public function getCatName()
	{
		return ProjectCategory::with($this->attributes['cat_id'])->find('display_name');
	}

	public function getCat()
	{
		return ProjectCategory::with($this->attributes['cat_id'])->find('name');
	}

}