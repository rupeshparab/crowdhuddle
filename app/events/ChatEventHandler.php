<?php

class ChatEventHandler {

    CONST EVENT = 'chat.update';
    CONST CHANNEL = 'chat.update';
    
    public function handle($data)
    {
        $redis = Illuminate\Support\Facades\Redis::connection();
        Log::info('This is some useful information. '.$data);
        $redis->publish(self::CHANNEL, $data);
    }
} 