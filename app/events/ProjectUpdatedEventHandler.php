<?php

class ProjectUpdatedEventHandler {

    CONST EVENT = 'projects.update';
    CONST CHANNEL = 'projects.update';
    
    public function handle($data)
    {
        $redis = Illuminate\Support\Facades\Redis::connection();
        Log::info('This is some useful information. '.$data);
        $redis->publish(self::CHANNEL, $data);
    }
} 