<?php

class UpdateScoreEventHandler {

    CONST EVENT = 'score.update';
    CONST CHANNEL = 'score.update';

    public function handle($data)
    {
        $redis = Illuminate\Support\Facades\Redis::connection();
        $redis->publish(self::CHANNEL, $data);
    }
}